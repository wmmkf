<?php
// admin.php:
$lang_add['admin_area'] =                     "Admin area";
$lang_add['back'] =                           "Back";
$lang_add['user_administr'] =                 "User administration";
$lang_add['banlists'] =                       "Banlists and not accepted words";
$lang_add['debug_administr'] =                "Debug modes";
$lang_add['category_administr'] =             "Category administration";
$lang_add['delete_category'] =                "Delete category";
$lang_add['forum_settings'] =                 "Forum settings";
$lang_add['search_user'] =                    "Search user:";
$lang_add['reg_user'] =                       "Register user";
$lang_add['ar_send_userdata'] =               "Send userdata to the above specified e-mail address";
$lang_add['register_exp'] =                   "Here you can manually register a user. You can leave blank the fields for the password and the password confirmation if the password should be generated automatically. In this case the userdata has to be sent to the specified e-mail adress.";
$lang_add['error_send_userdata'] =            "if you leave blank the password, the userdata has to be sent to the specified e-mail adress";
$lang_add['reg_another_user'] =               "Register another user";
$lang_add['del_marked'] =                     "Delete marked threads";
$lang_add['lock_marked'] =                    "Lock marked threads";
$lang_add['unlock_marked'] =                  "Unlock marked threads";
$lang_add['unmark_threads'] =                 "Unmark threads";
$lang_add['invert_markings'] =                "Invert markings";
$lang_add['mark_threads'] =                   "Mark threads";
$lang_add['backup_restore'] =                 "Backup or restore database";
$lang_add['backup'] =                         "Backup:";
$lang_add['restore'] =                        "Restore:";
$lang_add['import_sql'] =                     "Import SQL dump";
$lang_add['import_sql_note'] =                "There has to be one query in each line. The code will be executed as it is so be sure it's correct!";
$lang_add['sql_dump'] =                       "SQL dump:";
$lang_add['mysql_error'] =                    "MySQL-Error: ";
$lang_add['import_sql_ok'] =                  "The code has been executed.";
$lang_add['settings_sb'] =                    "OK - Save settings";
$lang_add['user_id'] =                        "ID";
$lang_add['user_name'] =                      "User name";
$lang_add['user_email'] =                     "E-Mail";
$lang_add['user_type'] =                      "User type";
$lang_add['user_registered'] =                "Registered";
$lang_add['user_logins'] =                    "logins";
$lang_add['last_login'] =                     "Last login";
$lang_add['mailto_user_lt'] =                 "E-mail to [name]";
$lang_add['change_user_type_lt'] =            "Change user type of [name]";
$lang_add['delete_users_sb'] =                "Delete";
$lang_add['delete_users_sb_title'] =          "Delete selected users";
$lang_add['delete_users_hl'] =                "Delete user";
$lang_add['delete_user_conf'] =               "Do you really want to delete this user?";
$lang_add['delete_users_conf'] =              "Do you really want to delete the following users?";
$lang_add['debug_none'] =                     "Debug mode: off";
$lang_add['debug_none_d'] =                   "No debugging informations (normal behaviour)";
$lang_add['debug_lang'] =                     "Debug mode: language";
$lang_add['debug_lang_d'] =                   "Delivers debugging informations about language strings as tool tip";
$lang_add['debug_session'] =                  "Debug mode: session";
$lang_add['debug_session_d'] =                "Delivers debugging informations about the session and cookies";
$lang_add['debug_css'] =                      "Debug mode: CSS";
$lang_add['debug_css_d'] =                    "Activates an CSS test file. All standard-CSS rules are inactive.";

$lang_add['0'] =                              "off";
$lang_add['1'] =                              "on";
$lang_add['no'] =                             "no";
$lang_add['yes'] =                            "yes";

# general settings
$lang_add['forum_name'] =                     "Forum name";
$lang_add['forum_name_d'] =                   "will be (amongst others) shown in the header";
$lang_add['forum_email'] =                    "Forum e-mail address";
$lang_add['forum_email_d'] =                  "will be used as contact address and forwarding e-mail address for all e-mails sent by the forum";
$lang_add['forum_disabled'] =                 "Disable forum";
$lang_add['forum_disabled_d'] =               "Do you want to make the forum unavailable?";
$lang_add['forum_address'] =                  "Forum address";
$lang_add['forum_address_d'] =                "URL of the forum (use this format: http://www.domain.tld/forum/)";
$lang_add['language_file'] =                  "Language";
$lang_add['language_file_d'] =                "Language of the Forum";
# old
$lang_add['home_link'] =                      "Home link";
$lang_add['home_link_d'] =                    "Link to your homepage, e.g. \"http://www.domain.tld/\" or \"../index.html\" (optional)";
# new
$lang_add['home_linkaddress'] =               "Home link";
$lang_add['home_linkaddress_d'] =             "Link to your homepage, e.g. \"http://www.domain.tld/\" or \"../index.html\" (optional)";
# old
$lang_add['home_link_name'] =                 "Home link name";
$lang_add['home_link_name_d'] =               "Name of the home link, e.g. &quot;Back to homepage&quot;";
# new
$lang_add['home_linkname'] =                  "Home link name";
$lang_add['home_linkname_d'] =                "Name of the home link, e.g. &quot;Back to homepage&quot;";
# old
$lang_add['template_file'] =                  "Template file";
$lang_add['template_file_d'] =                "Template for the page layout";
# new
$lang_add['template'] =                       "Template file";
$lang_add['template_d'] =                     "Template for the page layout";
# old
$lang_add['rss_feed'] =                       "RSS feed:";
$lang_add['rss_feed_d'] =                     "Should a RSS feed with the newest posts be offered (only possible if access permission (cp. above) has been set to \"Everyone\")?";
#new
$lang_add['provide_rssfeed'] =                "RSS feed:";
$lang_add['provide_rssfeed_d'] =              "Should a RSS feed with the newest posts be offered (only possible if access permission (cp. above) has been set to \"Everyone\")?";
# old
$lang_add['forum_time_difference'] =          "Forum time difference";
$lang_add['forum_time_difference_d'] =        "Time difference between server and forum time";
# new
$lang_add['time_difference'] =                "Forum time difference";
$lang_add['time_difference_d'] =              "Time difference between server and forum time";
# old
$lang_add['accession'] =                      "Access permission";
$lang_add['accession_d'] =                    "Who has access to the forum?";
# new
$lang_add['access_for_users_only'] =          "Access permission";
$lang_add['access_for_users_only_d'] =        "Who has access to the forum?";

$lang_add['access_only_reg_users'] =          "Only registered users";
$lang_add['access_all_users'] =               "Everyone";

$lang_add['user_edit'] =                      "Edit posting by user";
$lang_add['user_edit_d'] =                    "Should a user be able to edit his posting?";
$lang_add['user_delete'] =                    "Delete posting by user";
$lang_add['user_delete_d'] =                  "Should a user be able to delete his posting?";
$lang_add['mail_parameter'] =                 "Mail parameter";
$lang_add['mail_parameter_d'] =               "Parameter to contact the mail server (maybe needed). Contact your provider for information.";
$lang_add['session_prefix'] =                 "Session prefix";
$lang_add['session_prefix_d'] =               "Use the prefix to distinguish the forum sessions from other systems sessions.";
$lang_add['version'] =                        "Software version";
$lang_add['version_d'] =                      "Version string of the current installation.";
$lang_add['server_timezone'] =                "Server time zone";
$lang_add['server_timezone_d'] =              "Time zone of the web server.";
$lang_add['autologin'] =                      "Auto login";
$lang_add['autologin_d'] =                    "Use cookies to log in automatically when a user opens the forum.";

# view settings
$lang_add['thread_view'] =                    "Thread view";
$lang_add['thread_view_d'] =                  "Do you wish to use the thread view?";
$lang_add['board_view'] =                     "Board view";
$lang_add['board_view_d'] =                   "Do you wish to use the board view?";
$lang_add['mix_view'] =                       "Mix view";
$lang_add['mix_view_d'] =                     "Do you wish to use the mix view?";
$lang_add['standard'] =                       "Default view";
$lang_add['standard_d'] =                     "Which view should be used as default?";
$lang_add['standard_thread'] =                "Thread view";
$lang_add['standard_board'] =                 "Board view";
$lang_add['standard_mix'] =                   "Mix view";
$lang_add['entry_perm'] =                     "Entries";
$lang_add['entry_perm_d'] =                   "Who is allowed to make entries?";
$lang_add['register_perm'] =                  "User registration";
$lang_add['register_only_admin'] =            "Only by admin";
$lang_add['register_self'] =                  "Self";
$lang_add['register_perm_d'] =                "Can users register themselves?";
$lang_add['bbcode'] =                         "BB code";
$lang_add['bbcode_d'] =                       "Should BB codes for formatting message texts be allowed ([b]bold[/b], [i]italic[/i], [link]http://www.domain.com/[/link], [link=http://www.domain.tld/]Link[/link])?";
# old
$lang_add['mark_reg_users'] =                 "Mark registered users";
$lang_add['mark_reg_users_d'] =               "Should registered users be recognisable by a mark?";
# new
$lang_add['show_registered'] =                "Mark registered users";
$lang_add['show_registered_d'] =              "Should registered users be recognisable by a mark?";

$lang_add['last_reply_link'] =                "Direct link to last answer";
$lang_add['last_reply_link_d'] =              "Link to the last answer in the board view";
$lang_add['last_reply_name'] =                "Posters name of the last answer";
$lang_add['last_reply_name_d'] =              "Refer the posters name of the last answer in the board view";
$lang_add['show_posting_id'] =                "Show the posting ID";
$lang_add['show_posting_id_d'] =              "Show the ID of a posting i.e. for use in links.";
$lang_add['asc'] =                            "new posting bottom";
$lang_add['desc'] =                           "new posting on top";

$lang_add['all_views_direct'] =               "Direct access to all views";
$lang_add['all_views_direct_d'] =             "Should an user be able to open a thread in all accessible views?";
$lang_add['thread_depth_indent'] =            "Depth of thread indent";
$lang_add['thread_depth_indent_d'] =          "In wich depth should the indention of threads end?";
$lang_add['empty_postings_possible'] =        "Empty postings possible";
$lang_add['empty_postings_possible_d'] =      "Should an user be able to post an empty posting?";
$lang_add['admin_mod_highlight'] =            "Highlight admins and moderators";
$lang_add['admin_mod_highlight_d'] =          "Mark admins and moderators names with differing colours.";
$lang_add['user_highlight'] =                 "Highlight registered users";
$lang_add['user_highlight_d'] =               "Mark names of registered users with differing colours.";
$lang_add['thread_view_sorter'] =             "Sorting of answers in thread view";
$lang_add['thread_view_sorter_d'] =           "Sets the sort order in thread view. The newest answers to a topic can be set to top or bottom of the thread.";

$lang_add['topics_per_page'] =                "Threads/Topics per page";
$lang_add['topics_per_page_d'] =              "How many threads/topics should be displayed per page?";
$lang_add['email_notification'] =             "E-mail notification to the posting author";
$lang_add['email_notification_d'] =           "Option for e-mail notification to the posting author once a reply to this post has been submitted";

# posting settings
$lang_add['edit_own_entries'] =               "Editing own posts";
$lang_add['edit_own_entries_d'] =             "Should registered users be allowed to edit their own posts?";
$lang_add['show_if_edited'] =                 "Show editing";
$lang_add['show_if_edited_d'] =               "Show whether, when and by whom a post has been edited?";

# old
$lang_add['admin_unnoticeable_edit'] =        "Unnoticed editing by admin";
$lang_add['admin_unnoticeable_edit_d'] =      "Should admins be allowed to edit posts &quot;unnoticed&quot;?";
# new
$lang_add['dont_reg_edit_by_admin'] =         "Unnoticed editing by admin";
$lang_add['dont_reg_edit_by_admin_d'] =       "Should admins be allowed to edit posts &quot;unnoticed&quot;?";
# old
$lang_add['mod_unnoticeable_edit'] =          "Unnoticed editing by moderators";
$lang_add['mod_unnoticeable_edit_d'] =        "Should moderators be allowed to edit posts &quot;unnoticed&quot;?";
# new
$lang_add['dont_reg_edit_by_mod'] =           "Unnoticed editing by moderators";
$lang_add['dont_reg_edit_by_mod_d'] =         "Should moderators be allowed to edit posts &quot;unnoticed&quot;?";

$lang_add['edit_period'] =                    "Edit Period";
$lang_add['edit_period_d'] =                  "How long can own entries be edited by users (in minutes, 0 for unlimited)?";
$lang_add['edit_delay'] =                     "Edit delay";
$lang_add['edit_delay_d'] =                   "If editing is to be shown, a time period (minutes) can be set within messages can be edited without showing it";
$lang_add['smilies'] =                        "Smilies";
$lang_add['smilies_d'] =                      "Should smilies be used?";
$lang_add['bbcode_img'] =                     "BB code for images";
$lang_add['bbcode_img_d'] =                   "Should it be allowed to add images to the posts ([img]http://www.domain.tld/image.jpg[/img]?";
$lang_add['upload_images'] =                  "Upload Images";
$lang_add['upload_images_d'] =                "Should it be possible to upload Images? Note: the folder img/uploaded/ needs rights to write (CHMOD 777)!";
$lang_add['autolink'] =                       "Auto link";
$lang_add['autolink_d'] =                     "Should links be automatically recognised and made clickable for the user?";
$lang_add['count_views'] =                    "Views counter";
$lang_add['count_views_d'] =                  "Should post views (hits) be counted?";
$lang_add['count_users_online'] =             "Users online counter";
$lang_add['count_users_online_d'] =           "Count the users which are currently online?";
$lang_add['captcha'] =                        "CAPTCHA:";
$lang_add['captcha_d'] =                      "Which forms should be protected by a CAPTCHA (only for not registered users)?";
$lang_add['captcha_posting'] =                "Posting form";
$lang_add['captcha_contact'] =                "Contact form";
$lang_add['captcha_register'] =               "Register form";
$lang_add['captcha_type'] =                   "CAPTCHA type:";
$lang_add['captcha_type_d'] =                 "Which CAPTCHA type should be used? Note that the graphical CAPTCHA requires the GD Lib and at least one TTF font. Put the font files (*.ttf) into the folder captcha/fonts/. You can get fonts e.g. on free-fonts.com.";
$lang_add['captcha_type_image'] =             "Graphical";
$lang_add['captcha_type_math'] =              "Mathematical";
$lang_add['empty_forum'] =                    "Empty forum";
$lang_add['uninstall'] =                      "Uninstall";
$lang_add['empty_forum_note'] =               "Are you sure you want to delete all entries irretrievably?";
$lang_add['empty_forum_sb'] =                 "OK - Delete all posts";
$lang_add['delete_db_note'] =                 "This function is used for uninstalling the forum if it will not be used anymore. The database tables will be deleted. All entries and user data will be lost. The forum will not work anymore after this function has been executed!";
$lang_add['delete_tables'] =                  "Delete forum tables (normally it's recommendet to choose this function to uninstall the forum!)";
$lang_add['delete_db'] =                      "Delete database <b>[database]</b> completely (only if there are no other tables in this database!)";
$lang_add['delete_db_note_sb'] =              "OK - Delete database";
$lang_add['tables_deleted'] =                 "The tables have been deleted successfully.";
$lang_add['tables_deleted_error'] =           "An error occurred while deleting the tables.";
$lang_add['db_deleted'] =                     "The database has been deleted successfully.";
$lang_add['db_deleted_error'] =               "An error occurred while deleting the database.";
$lang_add['del_marked_note'] =                "Do you really want to delete all threads market with [marked_symbol] ?";
$lang_add['del_marked_sb'] =                  "OK - Delete marked threads";
$lang_add['unmark_threads_note'] =            "Unmark all threads?";
$lang_add['invert_markings_note'] =           "Invert markings (mark all non-marked threads and unmark all marked threads)?";
$lang_add['mark_old_threads'] =               "Mark all threads which are older than the newest [number] threads.";
$lang_add['mark_old_threads_no_replies'] =    "Mark all threads without replies which are older than the newest [number] threads.";
$lang_add['lock_marked_conf'] =               "Lock all threads marked with [marked_symbol] ?";
$lang_add['unlock_marked_conf'] =             "Unlock all threads marked with [marked_symbol] ?";
$lang_add['pw_marking'] =                     "Password:";
$lang_add['pw_conf_marking'] =                "Confirm password:";
$lang_add['new_user_registered'] =            "User &quot;[name]&quot; registered.";
$lang_add['userdata_send_error'] =            "Note: Userdata could not be sent to the user!";
$lang_add['error_pw_conf_wrong'] =            "The password is not the same as the repeated one";
$lang_add['rename_user_m'] =                  "Rename user";
$lang_add['email_list'] =                     "E-mail address list of users";
$lang_add['no_categories'] =                  "No categories applied.";
$lang_add['cat_accession_all'] =              "All";
$lang_add['cat_accession_reg_users'] =        "Registered users";
$lang_add['cat_accession_mod_admin'] =        "Mods and admins";
$lang_add['cat_hl'] =                         "Category";
$lang_add['cat_accessible'] =                 "Accessible for";
$lang_add['cat_topics'] =                     "Topics";
$lang_add['cat_entries'] =                    "Messages";
$lang_add['cat_actions'] =                    "Actions";
$lang_add['cat_edit'] =                       "edit";
$lang_add['cat_delete'] =                     "delete";
$lang_add['cat_move'] =                       "Order";
$lang_add['cat_edit_hl'] =                    "Edit category";
$lang_add['edit_category'] =                  "Category:";
$lang_add['del_cat_hl'] =                     "Delete category \"[category]\"";
$lang_add['del_cat_completely'] =             "Delete category and all its entries";
$lang_add['del_cat_keep_entries'] =           "Delete category but keep its entries";
$lang_add['del_cat_move_entries'] =           "and move them into [category]";
$lang_add['del_cat_sb'] =                     "OK - Delete category";
$lang_add['new_category'] =                   "New category:";
$lang_add['category_already_exists'] =        "This category already exists";
$lang_add['entries_in_not_ex_cat'] =          "<b>Notice:</b> There are entries which can't be displayed because they are in a not existing category. What do you want to do with these entries?";
$lang_add['entries_in_not_ex_cat_delete'] =   "delete them";
$lang_add['entries_in_not_ex_cat_move'] =     "move them into [category]";
$lang_add['accessible_for'] =                 "Accessible for:";
$lang_add['sql_complete'] =                   "Complete SQL dump";
$lang_add['sql_forum'] =                      "SQL dump of entries";
$lang_add['sql_forum_marked'] =               "SQL dump of marked entries";
$lang_add['sql_userdata'] =                   "SQL dump of userdata";
$lang_add['sql_categories'] =                 "SQL dump of categories";
$lang_add['sql_settings'] =                   "SQL dump of settings";
$lang_add['sql_smilies'] =                    "SQL dump of smilies";
$lang_add['sql_banlists'] =                   "SQL dump of banlists";
$lang_add['clear_userdata'] =                 "Clear up userdata";
$lang_add['clear_userdata_expl'] =            "Deletion of users which fulfil the following conditions (mods and admins will not be deleted, downward the criteria to delete gets sharper):";
$lang_add['clear_users_1'] =                  "Delete users which have never logged in and registered more than 2 days ago";
$lang_add['clear_users_2'] =                  "Delete users which fulfil the condition above or logged in maximal one time and the last time more than 30 days ago";
$lang_add['clear_users_3'] =                  "Delete users which fulfil the condition above or logged in maximal 3 times and the last time more than 30 days ago";
$lang_add['clear_users_4'] =                  "Delete users which fulfil the condition above or logged in the last time more than 60 days ago";
$lang_add['clear_users_5'] =                  "Delete users which fulfil the condition above or logged in the last time more than 30 days ago";
$lang_add['no_users_in_sel'] =                "No users in selection / no users deleted";
$lang_add['banned_users'] =                   "Banned users";
$lang_add['banned_users_d'] =                 "Users who should not have access to the forum. To specify several users, separate them with commas.";
$lang_add['banned_ips'] =                     "Banned IPs";
$lang_add['banned_ips_d'] =                   "IP adresses which should not have access to the forum. To specify several IP addresses, separate them with commas.";
$lang_add['not_accepted_words'] =             "Not accepted words";
$lang_add['not_accepted_words_d'] =           "Words that will not be accepted in postings. These can also be domains to protect the forum from spamming. To specify several words, separate them with commas.";
$lang_add['banlists_submit'] =                "OK - Save";
$lang_add['enable_smilies'] =                 "enable smilies";
$lang_add['disable_smilies'] =                "disable smilies";
$lang_add['smilies_disabled'] =               "Smilies are disabled.";
$lang_add['edit_smilies_smiley'] =            "Smiley";
$lang_add['edit_smilies_codes'] =             "Code(s)";
$lang_add['edit_smilies_title'] =             "Title";
$lang_add['edit_smilies_action'] =            "Action";
$lang_add['edit_smilies_order'] =             "Order";
$lang_add['no_smilies'] =                     "No smilies specified.";
$lang_add['add_smiley_file'] =                "Add smiley:";
$lang_add['add_smiley_code'] =                "Code:";
$lang_add['no_other_smilies_in_folder'] =     "To add smilies, load up smiley files into the smilies folder (img/smilies).";
$lang_add['smiley_file_doesnt_exist'] =       "Smiley file doesn't exist";
$lang_add['smiley_code_error'] =              "No smiley code entered";
$lang_add['edit_smiley_hl'] =                 "Edit smiley";
$lang_add['edit_link'] =                      "edit";
$lang_add['delete_link'] =                    "delete";
$lang_add['delete_user'] =                    "Delete user";
$lang_add['edit_user'] =                      "Edit user";
$lang_add['usertype_marking'] =               "User Type:";

// timedifference:
$lang_add['td_title'] =                       "Time difference";
$lang_add['td_desc'] =                        "Set the difference to the server time in hours (requires cookies, inoperable for registered users - use user-settings instead):";
$lang_add['td_user_note'] =                   "This is inoperable for registered users. Use user-settings instead.";

// install.php:
$lang_add['install_title'] =                  "Forum installation";
$lang_add['language_file_inst'] =             "Please select the language of the forum:";
$lang_add['installation_mode_inst'] =         "Installation mode:";
$lang_add['installation_mode_installation'] = "Installation";
$lang_add['installation_mode_update'] =       "Update";
$lang_add['installation_instructions'] =      "Please fill in all form fields. If you didn't edit the file <b>db_settings.php</b> manually it needs write permission (CHMOD 666, you can set this with your FTP programm).";
$lang_add['update_instructions'] =            "To update the forum control the access data of the existing database. New tables will be created and their names will be stored in the <b>db_settings.php</b>. If you didn't edit the file <b>db_settings.php</b> manually it needs write permission (CHMOD 666, you can set this with your FTP programm).";
$lang_add['update_current_dbsettings'] =      "Current database settings";
$lang_add['update_current_dbtables'] =        "Current database tables";
$lang_add['update_news_message_1.8'] =        "Two additional tables will be created. One is for users own settings, one for administrating the settings, the user is allowed to use. Additionally there are settings in the new tables and some new settings in the settings table.";
$lang_add['update_new_dbtables'] =            "New database tables";
$lang_add['inst_basic_settings'] =            "Basic settings";
$lang_add['inst_main_settings_d'] =           "You can make further settings after the installation.";
$lang_add['inst_admin_settings'] =            "Administrator";
$lang_add['inst_admin_settings_d'] =          "Data of the forum administrator. With the admin name and password you can log in as forum administrator.";
$lang_add['inst_db_settings'] =               "Database";
$lang_add['inst_db_settings_d'] =             "The forum requires a MySQL database. Your webhoster probably already applied one. Specify the name and the access data of this database (host, name, user, password). The specified tables (settings table, forum table, category table, userdata table and users-online table - you can leave the names for the tables as they are if they're still not allocated within the database).<br /><u>For experts</u>: If you have the permission, you also can create a new database and if you already edited the file db_settings.php it's not necessary to overwrite it (it doesn't require write permission then):";
$lang_add['create_database'] =                "create specified database (normally do <u>not</u> check this!)";
$lang_add['dont_overwrite_settings'] =        "don't overwrite db_settings.php (normally do <u>not</u> check this!)";
$lang_add['delete_2char_smilies'] =           "Should 2 chars smilies be deleted?";
$lang_add['delete_2char_smilies_d'] =         "They are often misinterpreted (i.e. with posted code examples). <strong>Attention:</strong> All Smilies, wich are stored in the second row, will be deleted! See also the Admin Menu->Smilies.";
$lang_add['inst_admin_name'] =                "Admin name";
$lang_add['inst_admin_name_d'] =              "Name of the forum administrator";
$lang_add['inst_admin_email'] =               "Admin e-mail";
$lang_add['inst_admin_email_d'] =             "E-mail address of the forum administrator";
$lang_add['inst_admin_pw'] =                  "Admin password";
$lang_add['inst_admin_pw_d'] =                "Password to log in as forum administrator";
$lang_add['inst_admin_pw_conf'] =             "Password confirmation";
$lang_add['inst_admin_pw_conf_d'] =           "Repeat the password";
$lang_add['inst_pw_conf_error'] =             "The Password doesn't match with the repeated password";
$lang_add['inst_db_host'] =                   "Database host";
$lang_add['inst_db_host_d'] =                 "host name, probably \"localhost\"";
$lang_add['inst_db_user'] =                   "Database user";
$lang_add['inst_db_user_d'] =                 "Username to access the database";
$lang_add['inst_db_pw'] =                     "Database password";
$lang_add['inst_db_pw_d'] =                   "Password to access the database";
$lang_add['inst_db_name'] =                   "Database name";
$lang_add['inst_db_name_d'] =                 "Name of the database";
$lang_add['inst_table_prefix'] =              "Table prefix";
$lang_add['inst_table_prefix_d'] =            "Prefix for tables in database";
$lang_add['create_db_error'] =                "The database couldn't be created";
$lang_add['db_connection_error'] =            "Database connection error - please check host, user and password.";
$lang_add['db_inexistent_error'] =            "Specified database doesn't exist - please check database name.";
$lang_add['db_alter_table_error'] =              "Error while altering table";
$lang_add['db_update_error'] =                   "Error while updating entry";
$lang_add['db_delete_entry_error'] =             "Error while deleting entry";
$lang_add['db_create_table_error'] =             "The table <b>[table]</b> couldn't be created";
$lang_add['db_insert_admin_error'] =             "The admin data couldn't be saved";
$lang_add['db_insert_settings_error'] =          "The setting <b>[setting]</b> couldn't be saved";
$lang_add['db_update_settings_error'] =          "The setting <b>[setting]</b> couldn't be updated";
$lang_add['db_read_settings_error'] =         "The setting couldn't be readed";
$lang_add['no_writing_permission'] =          "No permission to overwrite the file <b>db_settings.php</b> (set CHMOD 666, currently CHMOD is [CHMOD])";
$lang_add['no_version_found'] =               "No version information of the current installation found";
$lang_add['select_version'] =                 "Select the version of your current installation:";
$lang_add['version_already_installed'] =      "The new version is already installed";
$lang_add['version_not_supported'] =          "The version of the old installation is not supported by the update script.";
$lang_add['forum_install_ok'] =               "OK - Install forum";
$lang_add['forum_update_ok'] =                "OK - Update forum";
$lang_add['installation_complete'] =          "Installation successful!";
$lang_add['installation_complete_exp'] =      "As a precaution please now delete <b>install.php</b> if the forum is accessible via the internet and set CHMOD of <b>db_settings.php</b> on 644 again!";
$lang_add['update_complete'] =                "Update successful!";
$lang_add['installation_complete_link'] =     "&raquo; Go to the forum...";

// admin area categories
$lang_add['settings_cat']['general'] =        "General";
$lang_add['settings_cat']['views'] =          "Forum Views";
$lang_add['settings_cat']['postings'] =       "Postings";
$lang_add['settings_cat']['sessions'] =       "Sessions";
$lang_add['settings_cat']['security'] =       "Security";
$lang_add['settings_cat']['enhanced'] =       "Enhanced";
$lang_add['settings_cat']['userdata'] =       "Registration Data";
$lang_add['settings_cat']['logindata'] =      "Logins";
$lang_add['settings_cat']['actions'] =        "Actions";
?>
