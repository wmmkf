<?php
$db_settings['host'] = "localhost";
$db_settings['user'] = "";
$db_settings['pw'] = "";
$db_settings['db'] = "";
$db_settings['settings_table'] = "forum_settings";
$db_settings['forum_table'] = "forum_entries";
$db_settings['category_table'] = "forum_categories";
$db_settings['userdata_table'] = "forum_userdata";
$db_settings['smilies_table'] = "forum_smilies";
$db_settings['banlists_table'] = "forum_banlists";
$db_settings['banned_ips_table'] = "forum_banned_ips";
$db_settings['useronline_table'] = "forum_useronline";
$db_settings['usersettings_table'] = "forum_usersettings";
$db_settings['us_templates_table'] = "forum_fu_settings";
$db_settings['usersubscripts_table'] = "forum_subscripts";
?>
